﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace clockAnalog
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            int consoleSize = Math.Min(Console.LargestWindowWidth / 2, Console.LargestWindowHeight);
            //consoleSize = 30;
            Console.WindowHeight = consoleSize;
            Console.WindowWidth = consoleSize * 2;
            consoleSize -= 2;

            if (consoleSize % 2 == 0)
            {
                consoleSize--;
            }

            int yCentr = Round((float)consoleSize / 2), xCentr = yCentr;

            Draw(xCentr, yCentr);

            int radius = xCentr;
            DrawCircle(xCentr, yCentr, radius);

            double sizeArrowSecond = (float)radius / 100 * 95;
            double sizeArrowMinute = (float)radius / 100 * 80;
            double sizeArrowHour = (float)radius / 100 * 50;

            int xEndSecond = xCentr, yEndSecond = yCentr;
            int xEndMinute = xCentr, yEndMinute = yCentr;
            int xEndHour = xCentr, yEndHour = yCentr;
            while (true)
            {
                DrawLine(xCentr, yCentr, xEndSecond, yEndSecond, ConsoleColor.Black);
                DrawLine(xCentr, yCentr, xEndMinute, yEndMinute, ConsoleColor.Black);
                DrawLine(xCentr, yCentr, xEndHour, yEndHour, ConsoleColor.Black);
                DateTime time = DateTime.Now;

                double angleSecond = time.Second * 360 / 60;
                double radSecond = (angleSecond - 90) * Math.PI / 180;
                double angleMinute = time.Minute * 360 / 60;
                double radMinute = (angleMinute - 90) * Math.PI / 180;
                double angleHour = time.Hour * 360 / 12;
                double radHour = (angleHour - 90) * Math.PI / 180;

                xEndSecond = Round(xCentr + (sizeArrowSecond * Math.Cos(radSecond)));
                yEndSecond = Round(yCentr + (sizeArrowSecond * Math.Sin(radSecond)));
                xEndMinute = Round(xCentr + (sizeArrowMinute * Math.Cos(radMinute)));
                yEndMinute = Round(yCentr + (sizeArrowMinute * Math.Sin(radMinute)));
                xEndHour = Round(xCentr + (sizeArrowHour * Math.Cos(radHour)));
                yEndHour = Round(yCentr + (sizeArrowHour * Math.Sin(radHour)));

                DrawLine(xCentr, yCentr, xEndSecond, yEndSecond);
                DrawLine(xCentr, yCentr, xEndMinute, yEndMinute);
                DrawLine(xCentr, yCentr, xEndHour, yEndHour);

                Thread.Sleep(1000);
            }
        }

        static int Round(double value)
        {
            return (int)Math.Round(value, MidpointRounding.AwayFromZero);
        }

        static void Draw(int x, int y, ConsoleColor color)
        {
            x *= 2;
            Console.SetCursorPosition(x, y);
            Console.BackgroundColor = color;
            Console.Write("  ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(x, y);
        }

        static void Draw(int x, int y)
        {
            Draw(x, y, ConsoleColor.White);
        }

        static void DrawCircle(int xCentr, int yCentr, int rad, ConsoleColor color)
        {
            for (float x = rad * -1; x < rad; x++)
            {
                float y = Round(Math.Sqrt(Math.Pow(rad, 2) - Math.Pow(x, 2)));

                Draw(xCentr + Round(x), yCentr + Round(y), color);
                Draw(xCentr + Round(x), yCentr + Round(y) * -1, color);

                Draw(yCentr + Round(y), xCentr + Round(x), color);
                Draw(yCentr + Round(y) * -1, xCentr + Round(x), color);
            }
        }

        static void DrawCircle(int xCentr, int yCentr, int rad)
        {
            DrawCircle(xCentr, yCentr, rad, ConsoleColor.White);
        }

        static void DrawLine(int xCentr, int yCentr, int xEnd, int yEnd, ConsoleColor color)
        {
            float xRatio = 0;
            try
            {
                xRatio = Math.Abs((float)(xEnd - xCentr) / (yEnd - yCentr));
            }
            catch (Exception ex)
            { }

            for (int x = 0; x < Math.Abs(xEnd - xCentr); x++)
            {
                int y = 0;
                try
                {
                    y = Round(x / xRatio);
                }
                catch (Exception ex)
                { }

                int xx = xEnd - xCentr >= 0 ? x : x * -1;
                int yy = yEnd - yCentr >= 0 ? y : y * -1;

                Draw(Round(xCentr + xx), Round(yCentr + yy), color);
            }

            for (int y = 0; y < Math.Abs(yEnd - yCentr); y++)
            {
                int x = 0;
                try
                {
                    x = Round(y * xRatio);
                }
                catch (Exception ex)
                { }

                int xx = xEnd - xCentr >= 0 ? x : x * -1;
                int yy = yEnd - yCentr >= 0 ? y : y * -1;

                Draw(Round(xCentr + xx), Round(yCentr + yy), color);
            }
        }

        static void DrawLine(int xCentr, int yCentr, int xEnd, int yEnd)
        {
            DrawLine(xCentr, yCentr, xEnd, yEnd, ConsoleColor.White);
        }
    }
}
